import React from 'react';
import {Route, Switch} from 'react-router-dom';
import HomeComponent from '../components/Home/HomeComponent'
import LoginComponent from '../components/LoginComponent/LoginComponent';
import UserDashboard from '../components/UserDashboard/UserDashboard'

const RouteComponent = () => {
    return(
        <>
        <Switch>
        <Route exact path="/" component={HomeComponent}></Route>
        <Route path="/loginPage" component={LoginComponent}></Route>
        <Route path="/userdashboard" component={UserDashboard}></Route>
        </Switch>
        </>
    )
}

export default RouteComponent;
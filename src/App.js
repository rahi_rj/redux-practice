import React from 'react';
import './App.css';
import Header from './components/Header/Header';
import {BrowserRouter} from 'react-router-dom';
import RouteComponent from './routers/RouteComponent';


function App() {
  return (
    <BrowserRouter>
    <div>
      <Header />
      </div>
      <div>
      <RouteComponent />
      </div>
    </BrowserRouter>
  );
}

export default App;

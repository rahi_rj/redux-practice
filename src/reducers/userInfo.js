const initialState = {
    userDetails : {}
}

const UserInfo = (state = initialState, actions) => {
    switch(actions.type) {
        case 'SET_USERDATA' : return {...state, userDetails: actions.payload}
        default : return state
    }
}


export default UserInfo
import React from 'react';
import {Link} from 'react-router-dom';
import '../style.css';
import {connect} from 'react-redux';

function Header(props) {
    return (
        <div className="app-header">
            Redux Learing Application

            {props.data ? 
                 <button className="signin-btn">Log Out</button> 
                 : 
                 <button className="signin-btn"><Link to="/loginPage">Sign In</Link></button>
                }
            {/* <button className="signin-btn"><Link to="/loginPage">Sign In</Link></button> */}
        </div>
    )
}

const mapStateToProps = state => {
    return {
        data: state.UserInfo.userDetails.UserName
    }
}

export default  connect(mapStateToProps)(Header)
import React, { useState } from 'react';
import {connect} from 'react-redux';
import {updateUserData} from '../../actions/userAction';
import { bindActionCreators } from 'redux';

function LoginComponent(props){

    const [userData, setUserData]= useState([]);

    function changeHandler (e) {
        setUserData({...userData, [e.target.name] : e.target.value});        
    }

    function logData() {
        console.log(userData);
        if(userData.UserName==='rj' && userData.Password === 'rj'){
            alert('Successfully Logged In...')
            props.updateUserData(userData);
            props.history.push('/userdashboard');
        } 
        else{
            alert('Invalid Credential...')
        }       
    }

    return (
        <>
        {/* <p>{props.tempUserData.userDetails.UserName}</p> */}
        <div className="login-page">
            <h3>Enter Credentials to Sign In....</h3>  
            <hr className = "line"></hr>            
            <form className="login-form">
            <p>
                <label className="labels">User Name : <input type="text" name="UserName" 
				     onChange={changeHandler}></input>
	            </label>
            </p> 
            <p>  
                <label className="labels">Password : <input className="password-label" type="text" name="Password" 
                     onChange={changeHandler} ></input>
	            </label>
            </p>
            </form>
            <button className="login-btn" onClick={logData}>Sign In</button>
        </div>
        </>
    )
}

 /** mapStateToProps is used for selecting the part of the data from the store that the connected component needs.
  *  It's frequently referred to as just mapState for short. ... It receives the entire store state, and should return an object of data this component needs.
  *  Or in other Words, mapStateToProps is a function that you would use to provide the store data to your component, */

const mapStateToProps = state => {
    console.log("state", state)
   return {
       tempUserData : state.UserInfo
    }
}


/** mapDispatchToProps is something that you will use to provide the action creators as props to your component. */
const mapDispatchtoProps = dispatch => {
     return bindActionCreators({updateUserData}, dispatch)
}

export default connect(mapStateToProps,mapDispatchtoProps)(LoginComponent)

import { createStore, combineReducers } from 'redux';
import UserInfo from './../reducers/userInfo'

const store = createStore(combineReducers({
    UserInfo
}),{});


export default store;
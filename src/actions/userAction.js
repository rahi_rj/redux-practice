export const updateUserData = userData => (
    {
        type: 'SET_USERDATA',
        payload: userData
    }
)